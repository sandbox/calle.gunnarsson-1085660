(function($) {
	if (Drupal.jsEnabled) {
		Drupal.behaviors.taxonomy_assign = function(context) {
			$('#taxonomy-assign-term-form', context).submit(function() {
				$('select option').each(function(i) {
					$(this).attr("selected", "selected");
				});
			});
			$('#edit-add', context).bind('mousedown', function() {
				Drupal.attachBehaviors($(this));
				$('#edit-node-wrapper-available-nodes-fieldset-available-nodes-wrapper-available-nodes option:selected').each(function(i) {
					return $(this).remove().appendTo('#edit-node-wrapper-selected-nodes-fieldset-selected-nodes-wrapper-selected-nodes');
				});
			});
			$('#edit-remove', context).bind('mousedown', function() {
				Drupal.attachBehaviors($(this));
				$('#edit-node-wrapper-selected-nodes-fieldset-selected-nodes-wrapper-selected-nodes option:selected').each(function(i) {
					return $(this).remove().appendTo('#edit-node-wrapper-available-nodes-fieldset-available-nodes-wrapper-available-nodes');
				});
			});

			$('#action-move-up', context).click(function(event) {
				$('#edit-node-wrapper-selected-nodes-fieldset-selected-nodes-wrapper-selected-nodes option:selected').each(function(i) {
					$(this).insertBefore($(this).prev());
				});
			});
			$('#action-move-down', context).click(function(event) {
				$('#edit-node-wrapper-selected-nodes-fieldset-selected-nodes-wrapper-selected-nodes option:selected').each(function(i) {
					$(this).insertAfter($(this).next());
				});
			});
			$('#action-move-top', context).click(function(event) {
				$('#edit-node-wrapper-selected-nodes-fieldset-selected-nodes-wrapper-selected-nodes option:selected').each(function(i) {
					$(this).insertBefore($(this).siblings(":first"));
				});
			});
			$('#action-move-bottom', context).click(function(event) {
				$('#edit-node-wrapper-selected-nodes-fieldset-selected-nodes-wrapper-selected-nodes option:selected').each(function(i) {
					$(this).insertAfter($(this).siblings(":last"));
				});
			});
			$('.node-select-all', context).click(function(event) {
				$(this).parent('div').siblings('.form-item').find('select option').each(function(i) {
					$(this).attr("selected", "selected");
				});
			});
			$('.node-select-none', context).click(function(event) {
				$(this).parent('div').siblings('.form-item').find('select option').each(function(i) {
					$(this).removeAttr("selected");
				});
			});
			$('.node-select-inverse', context).click(function(event) {
				$(this).parent('div').siblings('.form-item').find('select option').each(function(i) {
					if ($(this).attr('selected')) {
						$(this).removeAttr("selected");
					} else {
						$(this).attr("selected", "selected");
					}
				});
			});
		}
	}
})(jQuery);

